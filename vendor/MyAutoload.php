<?php

declare(strict_types=1);

namespace Autoload;

class Autoloader
{
    private $prefixes = [];

    public function addNamespace(string $prefix, string $dir)
    {
        $prefix = trim($prefix, '\\') . '\\';
        $dir = rtrim($dir, \DIRECTORY_SEPARATOR) . \DIRECTORY_SEPARATOR;
        $this->prefixes[] = [$prefix, $dir];
    }

    public function register()
    {
        spl_autoload_register(array($this, 'autoload'));
    }

    public function findFile($class)
    {
        $class = ltrim($class, '\\');

        foreach ($this->prefixes as list($currentPrefix, $currentDir)) {
            if (0 === strpos($class, $currentPrefix)) {
                $classWithoutPrefix = substr($class, \strlen($currentPrefix));
                $file = $currentDir . str_replace('\\', \DIRECTORY_SEPARATOR, $classWithoutPrefix) . '.php';
                if (file_exists($file)) {
                    return $file;
                }
            }
        }

        return null;
    }

    public function autoload($class)
    {
        $file = $this->findFile($class);
        if (null !== $file) {
            include $file;

            return true;
        }

        return false;
    }
}

$autoloader = new Autoloader();
$autoloader->addNamespace('App', '../src');
$autoloader->addNamespace('App/Model', '../src');
$autoloader->register();
